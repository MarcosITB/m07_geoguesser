package com.example.androidquiz;

public class QuestionModel {

    private int numPreg;
    private int resp;

    public QuestionModel(int numPre, int res) {
        numPreg = numPre;
        resp = res;
    }

    public int getNumPreg() {
        return numPreg;
    }
    public int getresp() {
        return resp;
    }
    public void setNumPreg(int newNum) {
        this.numPreg = newNum;
    }
    public void setResp(int newResp) {
        this.resp = newResp;
    }


}
