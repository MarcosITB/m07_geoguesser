package com.example.androidquiz;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import java.util.List;
import java.util.Arrays;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    //private tal
    private Button Africa;
    private Button America;
    private Button Asia;
    private Button Europa;
    private TextView textobarra;
    private TextView pregunta;
    private ProgressBar progressBar;
    AlertDialog.Builder builder;
    private Integer numeroPregunta;
    Integer[] preguntasRepetidas = new Integer[11];
    List<Integer> list = Arrays.asList(preguntasRepetidas);

    private int respuesta;
    private int contador = 0;
    QuestionModel pregunta1 = new QuestionModel(1, 2);
    QuestionModel pregunta2 = new QuestionModel(2, 4);
    QuestionModel pregunta3 = new QuestionModel(3, 2);
    QuestionModel pregunta4 = new QuestionModel(4, 3);
    QuestionModel pregunta5 = new QuestionModel(5, 1);
    QuestionModel pregunta6 = new QuestionModel(6, 4);
    QuestionModel pregunta7 = new QuestionModel(7, 3);
    QuestionModel pregunta8 = new QuestionModel(8, 1);
    QuestionModel pregunta9 = new QuestionModel(9, 2);
    QuestionModel pregunta10 = new QuestionModel(10, 2);


    public int getRandomNumber() {
        return (int) (Math.random() *10);
    }

    public void pulsar() {

        numeroPregunta = getRandomNumber();
        while (list.contains(numeroPregunta)) {
            numeroPregunta = getRandomNumber();
        }

        switch (numeroPregunta) {
            case 0:
                pregunta.setText("A que continente pertenece Perú?");
                respuesta = pregunta1.getresp();
                break;
            case 1:
                pregunta.setText("A que continente pertenece Francia?");
                respuesta = pregunta2.getresp();
                break;
            case 2:
                pregunta.setText("A que continente pertenece Brasil?");
                respuesta = pregunta3.getresp();
                break;
            case 3:
                pregunta.setText("A que continente pertenece India?");
                respuesta = pregunta4.getresp();
                break;
            case 4:
                pregunta.setText("A que continente pertenece El Congo?");
                respuesta = pregunta5.getresp();
                break;
            case 5:
                pregunta.setText("A que continente pertenece Italia?");
                respuesta = pregunta6.getresp();
                break;
            case 6:
                pregunta.setText("A que continente pertenece Nepal?");
                respuesta = pregunta7.getresp();
                break;
            case 7:
                pregunta.setText("A que continente pertenece Nigeria?");
                respuesta = pregunta8.getresp();
                break;
            case 8:
                pregunta.setText("A que continente pertenece Canadá?");
                respuesta = pregunta9.getresp();
                break;
            case 9:
                pregunta.setText("A que continente pertenece Argentina?");
                respuesta = pregunta10.getresp();
                break;


        }
        list.set(contador, (numeroPregunta));

        if (contador == 10) {
            contador = 0;
            list.clear();
            Toast.makeText(getApplicationContext(), "Reiniciando", Toast.LENGTH_SHORT).show();
        }

        contador++;

        progressBar.setProgress(contador);
        textobarra.setText("pregunta "+ contador +" de 10");
    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Africa = findViewById(R.id.Africa);
        America = findViewById(R.id.America);
        Asia = findViewById(R.id.Asia);
        Europa = findViewById(R.id.Europa);
        textobarra = findViewById(R.id.textobarra);
        pregunta = findViewById(R.id.pregunta);
        progressBar = findViewById(R.id.progressBar);


        pulsar();


        //parte 2
        Africa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    if (respuesta == 1) {
                        Toast.makeText(getApplicationContext(), "Respuesta Correcta", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Respuesta Incorrecta", Toast.LENGTH_SHORT).show();
                    }
                pulsar();
                }

            });

        America.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (respuesta == 2) {
                    Toast.makeText(getApplicationContext(), "Respuesta Correcta", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Respuesta Incorrecta", Toast.LENGTH_SHORT).show();
                }
                pulsar();
            }
        });
        Asia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (respuesta == 3) {
                    Toast.makeText(getApplicationContext(), "Respuesta Correcta", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Respuesta Incorrecta", Toast.LENGTH_SHORT).show();
                }
                pulsar();
            }
        });
        Europa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (respuesta == 4) {
                    Toast.makeText(getApplicationContext(), "Respuesta Correcta", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Respuesta Incorrecta", Toast.LENGTH_SHORT).show();
                }
                pulsar();
            }
        });

    }
}